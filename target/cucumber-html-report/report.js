$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/FuncTest.feature");
formatter.feature({
  "name": "Funct1 APIs",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "validation post request of api users",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@try2"
    }
  ]
});
formatter.step({
  "name": "user wants to perform operation for URI \"\u003curi\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "name": "user set the header as below",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "headerkey",
        "headervalue"
      ]
    },
    {
      "cells": [
        "Content-Type",
        "application/json"
      ]
    }
  ]
});
formatter.step({
  "name": "user set required parameters as below",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "pathParamName",
        "pathParamValue",
        "queryParamName",
        "queryParamValue"
      ]
    },
    {
      "cells": [
        "id",
        "users",
        "",
        ""
      ]
    }
  ]
});
formatter.step({
  "name": "user want to perform \"\u003coperation\u003e\" for \"\u003cservice\u003e\" with \"\u003cbodyFilePath\u003e\" below parameters",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "xpath",
        "value"
      ]
    },
    {
      "cells": [
        "name",
        "nilesh"
      ]
    },
    {
      "cells": [
        "job",
        "engineer"
      ]
    }
  ]
});
formatter.step({
  "name": "webservice should respond responce code \"\u003ccode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "webservice should send response as following",
  "keyword": "Then ",
  "rows": [
    {
      "cells": [
        "xpath",
        "value"
      ]
    },
    {
      "cells": [
        "name",
        "nilesh"
      ]
    },
    {
      "cells": [
        "job",
        "engineer"
      ]
    }
  ]
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "operation",
        "uri",
        "service",
        "bodyFilePath",
        "code"
      ]
    },
    {
      "cells": [
        "POST",
        "baseURI2",
        "/api/{id}",
        "users_api/user2.json",
        "201"
      ]
    }
  ]
});
formatter.scenario({
  "name": "validation post request of api users",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@try2"
    }
  ]
});
formatter.step({
  "name": "user wants to perform operation for URI \"baseURI2\"",
  "keyword": "Given "
});
formatter.match({
  "location": "stepDefinitions.BaseDefinition.user_wants_to_perform_operation_for_URI(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user set the header as below",
  "rows": [
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "stepDefinitions.BaseDefinition.user_set_the_header_as_below(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user set required parameters as below",
  "rows": [
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "stepDefinitions.BaseDefinition.user_set_required_parameters_as_below(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user want to perform \"POST\" for \"/api/{id}\" with \"users_api/user2.json\" below parameters",
  "rows": [
    {},
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "stepDefinitions.BaseDefinition.user_want_to_perform_for_with_below_parameters(java.lang.String,java.lang.String,java.lang.String,io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "webservice should respond responce code \"201\"",
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefinitions.BaseDefinition.webservice_should_respond_responce_code(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "webservice should send response as following",
  "rows": [
    {},
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefinitions.BaseDefinition.webservice_send_response_as_following(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
});