package trytest;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;

public class NewJsonParser {
	
	public static final Configuration configuration = Configuration.builder()
			.jsonProvider(new JacksonJsonNodeJsonProvider())
			.mappingProvider(new JacksonMappingProvider()).build();
	
	public NewJsonParser() {
	
//	public static final Configuration configuration = Configuration.builder()
//	.jsonProvider(new JacksonJsonNodeJsonProvider())
//	.mappingProvider(new JacksonMappingProvider()).build();

	}
	
	public String parseJsonToValue(String filePath, String nodePath) throws IOException {
		String jsonValue = null;
		FileReader reader = new FileReader(filePath);
		String expectedJson = IOUtils.toString(reader);
		JsonNode updatedJson = JsonPath.using(configuration).parse(expectedJson).read(nodePath);
		String s =	JsonPath.using(configuration).parse(expectedJson).read(nodePath).toString();
		System.out.println(s);
		if(updatedJson.isArray()) {
			jsonValue = updatedJson.get(0).asText();
			
		} else {
			jsonValue = updatedJson.asText();
		}
		
		return jsonValue;
		
		}
	
	
	public String setJsonNodeInRequest(String filePath, String nodePath, String setValue) throws IOException {
		String jsonString = null;
		FileReader reader = new FileReader(filePath);
		String expectedJson = IOUtils.toString(reader);
		jsonString = JsonPath.using(configuration).parse(expectedJson).set(nodePath, setValue).jsonString();
		//System.out.println(jsonString);
		
		return jsonString;
		
	}
	
	public String setJsonNodeInRequest(String filePath, Map<String, Object> nodeMap) throws IOException {
		String jsonString = null;
		FileReader reader = new FileReader(filePath);
		String expectedJson = IOUtils.toString(reader);
		for(String key : nodeMap.keySet()) {
			expectedJson = JsonPath.using(configuration).parse(expectedJson).set(key, nodeMap.get(key)).jsonString();	
		}
		jsonString = expectedJson;
		System.out.println(jsonString);
		
		return jsonString;
		
	}
		
}
	
	
