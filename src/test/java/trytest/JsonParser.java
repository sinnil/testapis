package trytest;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import net.minidev.json.JSONObject;

public class JsonParser {
	
	public static JSONObject parseJSONFile(String filename) throws JSONException, IOException {
        String content = new String(Files.readAllBytes(Paths.get(filename)));
       // System.out.println(content);
        JSONObject json = new JSONObject(content);
     //   System.out.println(json.get("books/website"));  
        return json;
    }
	
	
	public static JSONObject updateJson(JSONObject obj, String keyString, Object newValue) throws Exception {
        JSONObject json = new JSONObject();
       
        Iterator iterator = obj.keys();          
        String key = null;
        while (iterator.hasNext()) {
            key = (String) iterator.next();
            // if the key is a string, then update the value
            if ((obj.optJSONArray(key) == null) && (obj.optJSONObject(key) == null)) { 
                if ((key.equals(keyString))) {
                    // put new value
                    obj.put(key, newValue);
                    return obj;
                }
            }

            // if it's jsonobject
            if (obj.optJSONObject(key) != null) {
                updateJson(obj.getJSONObject(key), keyString, newValue);
            }

            // if it's jsonarray
            if (obj.optJSONArray(key) != null) {
                JSONArray jArray = obj.getJSONArray(key);
                System.out.println(jArray.length());
                for (int i = 0; i < jArray.length(); i++) {
                	System.out.println(jArray.getJSONObject(i));
                    updateJson(jArray.getJSONObject(i), keyString, newValue);
                }
            }
        }
       // System.out.println(obj.toString());
        return obj;
    }
	
	
	public static JSONObject function(JSONObject obj, String keyMain,String valueMain, String newValue) throws Exception {
	    // We need to know keys of Jsonobject
	    JSONObject json = new JSONObject();
	    Iterator iterator = obj.keys();
	    String key = null;
	    while (iterator.hasNext()) {
	        key = (String) iterator.next();
	        // if object is just string we change value in key
	        if ((obj.optJSONArray(key)==null) && (obj.optJSONObject(key)==null)) {
	            if ((key.equals(keyMain)) && (obj.get(key).toString().equals(valueMain))) {
	                // put new value
	                obj.put(key, newValue);
	                return obj;
	            }
	        }

	        // if it's jsonobject
	        if (obj.optJSONObject(key) != null) {
	            function(obj.getJSONObject(key), keyMain, valueMain, newValue);
	        }

	        // if it's jsonarray
	        if (obj.optJSONArray(key) != null) {
	            JSONArray jArray = obj.getJSONArray(key);
	            
	            for (int i=0;i<jArray.length();i++) {
	            	
	                    function(jArray.getJSONObject(i), keyMain, valueMain, newValue);
	            }
	        }
	    }
	   // System.out.println(obj.toString());
	    return obj;
	}
	
	public static void writeinjson(JSONObject obj, String filepath) {
		try {
			FileWriter file = new FileWriter(filepath);
			file.write(obj.toString());
			file.flush();
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
