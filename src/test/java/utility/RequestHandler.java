package utility;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//import org.json.simple.JSONObject;
import org.junit.Assert;


import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;

import io.cucumber.datatable.DataTable;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.minidev.json.JSONObject;
//import net.minidev.json.JSONObject;

public class RequestHandler {
	
	public final String filePath = "./src/test/resources/apiRequestData/";    //users_api/user_api1.json";
	
	final static Logger logger = LogManager.getLogger(RequestHandler.class);
	
	private RestAssuredConfig restAssuredConfig = RestAssured.config.encoderConfig(new EncoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
	
	private RequestSpecification httpRequest = RestAssured.given().config(restAssuredConfig);
	
	private Response response;
	
	
	public RequestHandler() {
		
	}
	
	public void setBaseURI(String baseURI) {
		logger.info("Base URI is: "+ baseURI);
		
		httpRequest.baseUri(baseURI);
		
	}
	
	public void setHeaders(Map<String, String> headers) {
        logger.info("Header are set from map: "+ headers);
		
		httpRequest.headers(headers);
	}
	
	public RequestSpecification setPathParam(Map<String, String> pathParam) {
		logger.info("PathParam is set from Map: "+ pathParam);
		
		return httpRequest.pathParams(pathParam);
	}
	
	public RequestSpecification setQueryParam(Map<String, String> queryParam) {
		logger.info("queryParam is set from Map: "+ queryParam);
		if(queryParam.size()!=0) {
			httpRequest = httpRequest.queryParams(queryParam);
		}
		return httpRequest;
	}
	
	
//	public RequestSpecification addBaseURI(RequestSpecification httpRequest, String baseURI) {
//        logger.info("Header are set from map: "+ baseURI);
//		
//        httpRequest.baseUri(baseURI);
//        return httpRequest;
//	}
	
	
//	public RequestSpecification addHeaders(RequestSpecification httpRequest, Map<String, String> headers) {
//        logger.info("Header are set from map: "+ headers);
//		
//        httpRequest.headers(headers);
//        return httpRequest;
//	}
	
//	public RequestSpecification addQeryParam(RequestSpecification httpRequest, Map<String, String> queryParam) {
//		httpRequest.queryParams(queryParam);
//		return httpRequest;
//	}
	
//	public RequestSpecification addPathParam(RequestSpecification httpRequest, Map<String, String> pathParam) {
//		httpRequest.queryParams(pathParam);
//		return httpRequest;
//	}
	
	public int retriveResponseCode(Response response) {
		return response.getStatusCode();
	}
	
	public String retriveStatusBody(Response response) {
		return response.getBody().asString();
	}

	public void getRestRequest(String uri) {
		response = httpRequest.relaxedHTTPSValidation().when().get(uri);
		String res = response.body().asString();
		//System.out.println(res);
	}
	
	public void getRestRequestWithPathParam(String uri, Map<String, String> pathParam) {
		
		if(pathParam.size()!=0) {
		httpRequest = setPathParam(pathParam);
		}
		
		logger.info("Sending GET request with "+ uri);
	
		response = httpRequest.relaxedHTTPSValidation().when().get(uri);
//		String res = response.body().asString();
//		System.out.println(res);
		
	}
	
	public void setRequestParameters(DataTable dataTable) {
		Map<String, String> pathParam = DataTableReader.getPathParamDataTable(dataTable);
		Map<String, String> queryParam = DataTableReader.getQueryParamDataTable(dataTable);
		
		if(pathParam.size()!=0) {
			httpRequest = setPathParam(pathParam);
			}
		if(queryParam.size()!=0) {
			httpRequest = setQueryParam(queryParam);
			}
	}
	
	public void sendRequestWithParameter(String operation, String service, String bodypath, DataTable dataTable)  {

	//	Map<String, Object> bodymap = null; //JsonModifier.getMapFromJson(requestPath+bodaypath);
		Map<String, Object> bmap = null;
		String jsonString = null;
		String requestPath = null;

		if(operation.equals("GET")) {
			logger.info("Sending GET request with "+ service);
			response = httpRequest.relaxedHTTPSValidation().when().get(service);
		}
		if(operation.equals("POST")) {
			requestPath = filePath + bodypath;
//			JsonModifier.modifyJson(requestPath, dataTable);
//			bodymap = JsonModifier.getMapFromJson(requestPath);
//			//System.out.println(bodymap);
//			JSONObject requestParams = new JSONObject();
//			requestParams.putAll(bodymap);
//			httpRequest.body(requestParams.toJSONString());
			bmap = DataTableReader.getDataFromData(dataTable, "xpath", "value");
			jsonString = JsonModifier.setJsonNodeInRequest(requestPath, bmap);
			httpRequest.body(jsonString);
			logger.info("Sending POST request with "+ service);
			response = httpRequest.relaxedHTTPSValidation().when().post(service);
		}
		if(operation.equals("PUT")) {
			requestPath = filePath + bodypath;
			bmap = DataTableReader.getDataFromData(dataTable, "xpath", "value");
			jsonString = JsonModifier.setJsonNodeInRequest(requestPath, bmap);
			httpRequest.body(jsonString);
			logger.info("Sending PUT request with "+ service);
			response = httpRequest.relaxedHTTPSValidation().when().put(service);
		}
		if(operation.equals("DELETE")) {
			
			logger.info("Sending DELETE request with "+ service);
			response = httpRequest.relaxedHTTPSValidation().when().delete(service);
		}
		//String res = response.body().asString();
		//System.out.println(res);
		
	}
	
	public void getRequestWithPathParamAndQueryParam(String uri, DataTable dataTable) {
		Map<String, String> pathParam = DataTableReader.getPathParamDataTable(dataTable);
		Map<String, String> queryParam = DataTableReader.getQueryParamDataTable(dataTable);
		if(pathParam.size()!=0) {
			httpRequest = setPathParam(pathParam);
			}
		if(queryParam.size()!=0) {
			//System.out.println();
			httpRequest = setQueryParam(queryParam);
			}	
			logger.info("Sending GET request with "+ uri);
//			System.out.println(uri);
			response = httpRequest.relaxedHTTPSValidation().when().get(uri);
//			String res = response.body().asString();
//			System.out.println(res);
	}
	

	
	public void assertResponseCode(int expectedCode) {
		int actualCode = retriveResponseCode(response);
//		System.out.println(actualCode);
		Assert.assertEquals("Response code does not match", actualCode, expectedCode);
	}
	
	public Map<String, String> parseJsonPathToMap(String jsonPathTag){
		Map<String, String> jsonPathValue = null;
//		List<Map<String, String>> booksOfUser = JsonPath.from(response.body().asString()).get();
		jsonPathValue = response.jsonPath().getMap(jsonPathTag);
//		System.out.println(jsonPathValue);
		return jsonPathValue;
	}
	
	public String parseJsonPathToString(String jsonPathTag){
		String jsonPathValue = null;
//		List<Map<String, String>> booksOfUser = JsonPath.from(response.body().asString()).get();
		jsonPathValue = response.jsonPath().getString(jsonPathTag);
//		System.out.println(jsonPathValue);
		return jsonPathValue;
	}
	
	public Map<String, String> getJsonMap(DataTable dataTable) {
    	Map<String, String> jsonTagMap = DataTableReader.getDataFromDataTable(dataTable, "xpath", "value");
    	Map<String, String> jsonValueMap = new HashMap<>();
    	
    	for(String key: jsonTagMap.keySet()) {
    		jsonValueMap.put(key, parseJsonPathToString(key));
    	}
    	return jsonValueMap;
    }
	
	public void verifyJsonResponse(DataTable dataTable) {
		Map<String, String> actualJson = getJsonMap(dataTable);
//		System.out.println(actualJson);
		Map<String, String> expectedJson = DataTableReader.getDataFromDataTable(dataTable, "xpath", "value");
//		System.out.println(expectedJson);
		Assert.assertEquals("Response value is not matching with given value", expectedJson, actualJson);
	}
	

	
	public void setBody(String filePath) {
		FileReader fileReader = null;
		JSONObject requestParams = new JSONObject();
		//String fileOutput="";
		try {
			fileReader = new FileReader(filePath);
			
					
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	//	httpRequest.b
	}
	
}
