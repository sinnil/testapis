package utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
//import org.testng.asserts.Assertion;

public class BaseConfig {
	
	private static final String TEST_DEFAULTS = "UserTestDefault.properties";
	
	private static Properties defaultProperties = new Properties();
	
	final static Logger logger = LogManager.getLogger(BaseConfig.class);
	
	public static String enveronment = "";
	
	static {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(TEST_DEFAULTS));
			
			try {
				defaultProperties.load(reader);
				reader.close();
			}	catch (IOException e) {
				e.printStackTrace();
				logger.debug("Exception occurs while loading the property file " + TEST_DEFAULTS );
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.debug("File '{}' not found for " + TEST_DEFAULTS, e);
			throw new RuntimeException("Configuration.properties not found at " + TEST_DEFAULTS);
			
		} 	
	}
	
	public BaseConfig() {
		
	}
	
	public static String getProperty(String propertyName) {
		String propertyValue = null;
		if(defaultProperties.getProperty(propertyName) != null) {
			propertyValue = defaultProperties.getProperty(propertyName);
		}
//		else if(StringUtils.isEmpty(propertyValue)) {
//			propertyValue = defaultProperties.getProperty(propertyName);
//		}
		else {
			throw new RuntimeException("property name or value are not specified in the Configuration.properties file.");		
		}
	
		return propertyValue;
	}
	
	public String readFile(String fileName) {
		FileReader fileReader = null;
		String fileOutput="";
		try {
			fileReader = new FileReader(fileName);
			
			fileOutput = IOUtils.toString(fileReader);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
//		fileReader.close();
		IOUtils.closeQuietly(fileReader);
		}
		return fileOutput;
	}
	
	

}
