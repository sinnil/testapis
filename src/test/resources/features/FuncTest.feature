Feature: Funct1 APIs

#Background: User generates token for Authorisation
#Given I am an authorized user

@try
Scenario Outline: validation get request of api bookstore
Given user wants to perform operation for URI "<uri>"
When user set the header as below
|headerkey   |headervalue     |
|Content-Type|application/json|
When user set required parameters as below
|pathParamName|pathParamValue |
|id           |v1          |
When user want to perform "<operation>" for "<service>" with "<bodyFilePath>" below parameters
| |     # Need pass empty data table if you don't required to pass body table
Then webservice should respond responce code "<code>"
Then webservice should send response as following
|xpath          |value            |
|books[0].title |Git Pocket Guide |


Examples: 
|operation|uri     |service              |bodyFilePath|code|
|GET      |baseURI1|/BookStore/{id}/Books|            |200 |


@try
Scenario Outline: validation get request of api users
Given user wants to perform operation for URI "<uri>"
When user set the header as below
|headerkey   |headervalue     |
|Content-Type|application/json|
When user set required parameters as below
|pathParamName|pathParamValue |queryParamName|queryParamValue|
|id           |users          |id            | 1             |
When user want to perform "<operation>" for "<service>" with "<bodyFilePath>" below parameters
| |     # Need pass empty data table if you don't required to pass body table

Then webservice should respond responce code "<code>"
Then webservice should send response as following
|xpath           |value  |
|data.first_name |George |


Examples: 
|operation|uri     |service      |bodyFilePath|code|
|GET      |baseURI2|/api/{id}    |            |200 |


@try2
Scenario Outline: validation post request of api users
Given user wants to perform operation for URI "<uri>"
When user set the header as below
|headerkey   |headervalue     |
|Content-Type|application/json|
When user set required parameters as below
|pathParamName|pathParamValue |queryParamName|queryParamValue|
|id           |users          |            |              |
When user want to perform "<operation>" for "<service>" with "<bodyFilePath>" below parameters
|xpath    |value    |
|name     |nilesh   |
|job      |engineer |
Then webservice should respond responce code "<code>"
Then webservice should send response as following
|xpath    |value    |
|name     |nilesh |
|job      |engineer   |


Examples: 
|operation|uri     |service      |bodyFilePath             |code|
|POST     |baseURI2|/api/{id}    |users_api/user2.json |201 |




